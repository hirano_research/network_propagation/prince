# PRINCE

# データの準備

- PhenSim.tsv : disease-disease
  - `wget http://www.cs.tau.ac.il/~bnet/software/PrincePlugin/PhenSim.tsv`

- associations.txt : disease-gene
  - `wget http://www.cs.tau.ac.il/~bnet/software/PrincePlugin/associations.txt`

- ppinet : gene-gene
  - 最新: `wget http://cbdm-01.zdv.uni-mainz.de/~mschaefer/hippie/HIPPIE-current.mitab.txt`
  - 最新一つ前(oyamada): `wget http://cbdm-01.zdv.uni-mainz.de/~mschaefer/hippie/HIPPIE-2.1.mitab.txt`
  - 最古: `wget http://cbdm-01.zdv.uni-mainz.de/~mschaefer/hippie/HIPPIE-1.1.mitab.txt`

```
mkdir data
mv *tsv data/
mv *txt data/
```

# 使い方

## PRINCEの計算

- `mkdir result`
- 使用するppinetはmain.R内でコメントアウトで切り替える
- 調節パラメータ(alpha, c)はmain.R内でalpha_list, c_listを書き換える
- main.R内の「正解データ作成」のところを有効にする
- 実行: `Rscript main.R`
- resultディレクトリに予測スコアが保存される
  - 保存データ: (行・列)disease x GeneID。(要素)スコア。 各diseaseでソートしていない。

## ROC・PRCのAUC評価

- サンプルスクリプト: `auc.R`
